<?php

namespace common\modules\fts\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\modules\fts\Module;
use common\modules\fts\models\SearchForm;

class SearchFormWidget extends Widget
{
    public $model;
    public $action;
    public $formOptions;
    public $inputOptions;

    public function init()
    {
        parent::init();

        $this->action = '/' . Module::getInstance()->id;

        $this->formOptions = [];

        if (empty($this->model))
        {
            $this->model = new SearchForm();
        }
    }

    public function run()
    {
        return $this->render('search-form', [
            'model' => $this->model, 
            'action' => $this->action,
            'formOptions' => $this->formOptions,
            'inputOptions' => $this->inputOptions
        ]);
    }
}