<?php
use yii\helpers\Html;
use yii\helpers\Url;
rmrevin\yii\fontawesome\AssetBundle::register($this);
?>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "<?= URL::to('/', true) ?>",
  "potentialAction": [{
    "@type": "SearchAction",
    "target": "<?= rawurldecode(Url::to([$action, 'q' => '{search_term_string}'], true, false)) ?>",
    "query-input": "required name=search_term_string"
  }]
}
</script>
<?= Html::beginForm($action, 'get', $formOptions) ?>
<div class="form-group">
    <div class="input-group">
        <input type="text" class="form-control" name="q" value="<?= $model->q ?>" placeholder="<?= Yii::t('search', 'Search...') ?>">
        <span class="input-group-btn">
            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
        </span>
    </div>
</div>
<?= Html::endForm() ?>