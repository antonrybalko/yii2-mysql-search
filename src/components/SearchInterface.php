<?php

namespace common\modules\fts\components;

use common\modules\fts\models\SearchData;

interface SearchInterface {

    public function getSearchData(): SearchData;

}