<?php

/**
 * FtsBehavior - поведение сохранения данных из ActiveRecord в поисковый индекс.
 * Объект ActiveRecord должен содержать метод ftsInfo, возвращающий данные для сохранения в поисковый индекс.
 */
 
namespace common\modules\fts\components;

use Yii;
use yii\base\Behavior;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\modules\fts\Module;
use common\modules\fts\models\SearchIndex;
use common\modules\fts\components\SearchInterface;
 
class SearchBehavior extends Behavior
{
    public function attach($owner)
    {
        if ($owner instanceof SearchInterface) {
            parent::attach($owner);
        } else {
            throw new \Exception(__METHOD__ . ": " . get_class($owner) . "  must be instance of SearchInterface");
        }
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
        ];
    }
    
    /**
     * Добавляет данные в поисковом индексе для модели
     * @param \yii\base\Event $event
     */
    public function afterInsert($event)
    {
        $this->saveSearchData(true);
    }

    public function afterUpdate($event)
    {
        $this->saveSearchData(false);
    }
    
    public function afterDelete($event)
    {
        $this->deleteSearchData();
    }
     
    public function saveSearchData($insert = null)
    {
        $searchData = $this->owner->getSearchData();
        $useLabels = Module::getInstance()->useLabels;
        $searchData->title = $this->prepareText($searchData->title, $useLabels);
        $searchData->text = $this->prepareText($searchData->text, $useLabels);
        $searchData->tags = $this->prepareText($searchData->tags, $useLabels);
        if ($searchData->modelClass == null) {
            $searchData->modelClass = get_class($this->owner);
        }
        if ($searchData->modelId === null) {
            $searchData->modelId = $this->owner->primaryKey;
        }
        if ($insert === null) {
            if (SearchIndex::updateSearchData($searchData)) {
                return 1;
            } else {
                return SearchIndex::createSearchData($searchData);
            }
        } elseif ($insert) {
            return SearchIndex::createSearchData($searchData);
        } else {
            
        }
        return SearchIndex::updateSearchData($searchData);
    }
    
    public function deleteSearchData()
    {
        $searchData = $this->owner->getSearchData();
        return SearchIndex::deleteSearchData($searchData);
    }

    /**
     * Формирует текст для индексирования. Текст может быть задан в в иде массива атрибутов
     * [
     *		['name', 'description'],
     *		'price'
     * ]
     * @return string
     */
    public function prepareText($text, $useLabels = false)
    {
        if (is_string($text) || $text === null) {
            return $text;
        }

        // Get values from model attributes
        $parts = [];
        foreach ($text as $attribute) {
            if ($attribute == '') {
                continue;
            }
            $label = '';

            // If array given then prepare text for array otherwise get attribute value
            if (is_array($attribute)) {
                $value = $this->prepareText($attribute, $useLabels);
            } else {
                if ($useLabels && $this->owner instanceof Model) {
                    $label =  $this->owner->getAttributeLabel($attribute);
                }
                $value = ArrayHelper::getValue($this->owner, $attribute);
            }

            if ($value !== null) {
                $parts[] = $label . $value;
            }
        }
        // Join attribute values separated by point
        return implode('. ', $parts);
    }

    public function searchRelated()
    {
        $q = $this->prepareText($this->owner->getSearchData());
        return SearchIndex::find()->search($q)->andWhere(['not',['model_id' => $this->owner->primaryKey]]);
    }

}
