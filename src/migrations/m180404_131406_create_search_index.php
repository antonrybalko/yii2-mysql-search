<?php

use yii\db\Migration;
use common\modules\fts\models\SearchIndex;

/**
 * Handles the creation for table `search_index`.
 */
class m180404_131406_create_search_index extends Migration
{
    public function safeUp()
    {
        switch (Yii::$app->db->driverName) {
            case 'mysql':
            case 'mysqli':
                $this->createMySql();
                break;
            case 'pgsql':
                //$this->createPgSql();
                throw new \Exception('Not supported db driver');
                break;
            default: 
                throw new \Exception('Not supported db driver');
        }
    }

    protected function createMySql()
    {
        $this->createTable(SearchIndex::tableName(), [
            'id' => $this->primaryKey(),
            'model_class' => $this->string(500),
            'model_id' => $this->string(256),
            'title' => $this->string(500),
            'text' => $this->text(),
            'tags' => $this->text(),
            'fulltext(title), fulltext(text), fulltext(tags)'
        ]);
        /* not supporter by MySQL 5.5
        $this->createIndex('idx_search_index_unique',
            'search_index', 
            ['model_class', 'model_id'], 
            true);
        */
    }

    public function safeDown()
    {
        $this->dropTable(SearchIndex::tableName());
    }
}
