<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use common\modules\fts\components\SearchFormWidget;
use common\modules\fts\Module;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('search', "Search results for '{query}'", ['query' => Html::encode($searchModel->q)]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-index-index">

    <?= SearchFormWidget::widget(['model' => $searchModel]) ?>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => function ($searchIndex, $key, $index, $widget) {
            $model = $searchIndex->getModel();
            $html = '';
            $html .= Module::getInstance()->getSearchResult($model)
            return $html;
        },
        'pager' => ['options' => ['class' => 'pagination col-md-12']],
        'layout' => '<div class="row">{items}</div> {summary} {pager}',
        'summary' => Yii::t('search', 'Results {begin} - {end} of {totalCount} items'),
    ]); ?>

</div>
