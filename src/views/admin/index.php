<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;
use common\widgets\Alert;
use rmrevin\yii\fontawesome\FAS;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\fts\models\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

rmrevin\yii\fontawesome\AssetBundle::register($this);

$this->title = Yii::t('search', 'Search Index');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-index-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //echo  Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Yii::t('search', 'Not indexed: {count}', ['count' => $reindexForm->notIndexed]) ?>
    </p>

    <p>
        <?php $form = ActiveForm::begin([
            'layout' => 'inline'
        ]); ?>

        <?= Alert::widget() ?>

        <?= $form->field($reindexForm, 'index', [
                'inputTemplate' => '<div class="input-group">{input}<span class="input-group-btn">' 
                    . Html::submitButton(Yii::t('search', '{icon} Index', ['icon' => FAS::i('cog')]), ['class' => 'btn btn-primary', 'name' => 'submit', 'value' => 'reindex']) 
                    .'</span></div>'
        ])->label(false) ?>

        <?= Html::submitButton(Yii::t('search', '{icon} Reindex all', ['icon' => FAS::i('cog')]), ['class' => 'btn btn-primary', 'name' => 'submit', 'value' => 'reindex-all']) ?>
        <?= Html::a(Yii::t('search', '{icon} Delete all', ['icon' => FAS::i('trash')]), ['test'], ['class' => 'btn btn-danger']) ?>
        <?= Html::a(Yii::t('search', '{icon} Cleanup', ['icon' => FAS::i('eraser')]), ['cleanup'], ['class' => 'btn btn-default']) ?>

        <?php ActiveForm::end(); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'model_class',
            'model_id',
            'title',
            'text:ntext',
            'tags:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
