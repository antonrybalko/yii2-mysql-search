<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\fts\models\SearchIndex */

$this->title = Yii::t('search', 'Create Search Index');
$this->params['breadcrumbs'][] = ['label' => Yii::t('search', 'Search Index'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="search-index-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
