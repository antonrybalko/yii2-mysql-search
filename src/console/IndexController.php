<?php
namespace common\modules\fts\console;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\modules\fts\Module;
use common\modules\fts\components\SearchInterface;

class IndexController extends Controller
{
    public function actionIndex()
    {
        Yii::$app->db->createCommand('SET ft_min_word_len=2')->execute();
        $this->reindex();
    }
    
    public function reindex()
    {
        foreach(Module::getInstance()->getModelClasses() as $modelClass) {
            echo "$modelClass\n";
            $this->reindexModelClass($modelClass);
        }
    }
    
    public function reindexModelClass($modelClass)
    {
        if ((new \ReflectionClass($modelClass))->implementsInterface(SearchInterface::class))
        {
            $models = $modelClass::find();
            $modelCount = $models->count();
            $modelIndex = 0;
            Console::startProgress($modelIndex, $modelCount);
            foreach($models->all() as $model) {
                $model->saveSearchData();
                Console::updateProgress($modelIndex, $modelCount);
                $modelIndex += 1;
            }
            Console::endProgress();
        } else {
            throw new \Exception(__METHOD__ . ": " . $modelClass . " must be instance of SearchInterface");
        }
    }
}
