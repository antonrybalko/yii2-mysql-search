<?php

namespace common\modules\fts\controllers;

use Yii;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use common\modules\fts\models\SearchForm;

/**
 * Search results
 */
class SearchController extends Controller
{
    /**
     * Lists all SearchIndex models.
     * @return mixed
     */
    public function actionIndex($q)
    {
        $searchModel = new SearchForm();
        $searchModel->load(Yii::$app->request->queryParams);
        $searchModel->q = Html::encode($q);
        $dataProvider = $searchModel->search();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
