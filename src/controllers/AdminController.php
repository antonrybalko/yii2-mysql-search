<?php

namespace common\modules\fts\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\base\DynamicModel;
use common\modules\fts\models\SearchIndex;
use common\modules\fts\models\SearchIndexSearch;

/**
 * AdminController implements index/reindex functionality for fts index
 */
class AdminController extends Controller
{
    /**
     * Lists all SearchIndex models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchIndexSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $reindexForm = $this->reindex();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reindexForm' => $reindexForm,
        ]);
    }

    protected function reindex()
    {
        $reindexForm = new DynamicModel(['index', 'indexed', 'notIndexed']);
        $reindexForm->addRule(['index'], 'integer');
        
        if ($reindexForm->load(Yii::$app->request->post()) && $reindexForm->validate()) {
            if (Yii::$app->request->post('submit') == 'reindex-all') {
                $reindexForm->indexed =  (int) SearchIndex::reindex();
            } else {
                $reindexForm->indexed =  (int) SearchIndex::index($reindexForm->index);
            }
            if ($reindexForm->indexed) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Indexed: {count}', ['count' => $reindexForm->indexed]));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Indexed: {count}', ['count' => $reindexForm->indexed]));
            }
        }

        $reindexForm->notIndexed = SearchIndex::getNotIndexedCount();
        $reindexForm->index = min($reindexForm->notIndexed, 100);

        return $reindexForm;
    }

    /**
     * Displays a single SearchIndex model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SearchIndex model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SearchIndex();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SearchIndex model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SearchIndex model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionTest()
    {
        $deleted = Yii::$app->db->createCommand()->delete(SearchIndex::tableName())->execute();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted: {count}', ['count' => $deleted]));
        return $this->redirect(['index']);
    }

    public function actionCleanup()
    {
        $deleted = SearchIndex::cleanup();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Deleted: {count}', ['count' => $deleted]));
        return $this->redirect(['index']);
    }

    /**
     * Finds the SearchIndex model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SearchIndex the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SearchIndex::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
