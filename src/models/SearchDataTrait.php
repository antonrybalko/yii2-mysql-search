<?php

namespace common\modules\fts\models;

use Yii;
use common\modules\fts\Module;
use common\modules\fts\models\SearchData;

trait SearchDataTrait {

    public static function createSearchData(SearchData $searchData)
    {
        $model = new static();
        $model->populateWithSearchData($searchData);

        if (!$model->validate()) {
            //echo __METHOD__;
            //print_r($model->errors);
            return false;
        }

        return $model->save();
    }

    public static function updateSearchData(SearchData $searchData)
    {
        $model = static::findBySearchData($searchData)->one();
        if (empty($model)) {
            return false;
        }

        $model->populateWithSearchData($searchData);
        if (!$model->validate()) {
            return false;
        }

        return $model->save();
    }
    
    public static function deleteSearchData($searchData)
    {
        $model = static::findBySearchData($searchData)->one();

        if (!$model) {
            return false;
        }

        return $model->delete();
    }
    
    public static function findBySearchData($searchData)
    {
        return static::find()->where([
            'model_class' => $searchData->modelClass,
            'model_id' => $searchData->modelId,
        ]);
    }

    public function populateWithSearchData($searchData)
    {
        $this->model_id = (string)$searchData->modelId;
        $this->model_class = $searchData->modelClass;
        $this->title = Module::getInstance()->normalizeDocument($searchData->title);
        $this->text = Module::getInstance()->normalizeDocument($searchData->text);
        $this->tags = Module::getInstance()->normalizeDocument($searchData->tags);
    }
}