<?php

namespace common\modules\fts\models;

use yii\base\BaseObject;

class SearchData extends BaseObject {
    public $title;
    public $text;
    public $tags;
    public $modelClass;
    public $modelId;
}
