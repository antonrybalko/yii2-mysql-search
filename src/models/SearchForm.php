<?php

namespace common\modules\fts\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\fts\models\SearchIndex;

/**
 * SearchForm returns search results sorted by rank
 */
class SearchForm extends SearchIndex
{
    /**
     * @inheritdoc
     */
	 
    public $q;
    public $rank;
	
    public function rules()
    {
        return [
            [['q'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels()
    {
        return [
            'q' => Yii::t('search', 'Query'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        if (empty($this->q) || !$this->validate()) {
            // TODO: better code if we dont want to return any records when validation fails?
            $dataProvider = new ActiveDataProvider([
                'query' => static::find()->where('0=1'),
            ]);
            return $dataProvider;
        }
        // Search method adds full text search condition
        $query = static::find()->search($this->q);
        $query->andFilterWhere([
            'model_class' => $this->model_class,
            'model_id' => $this->model_id,
        ]);

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }
}
