<?php

namespace common\modules\fts\models\drivers;

use yii\db\ActiveQuery;
use yii\db\Expression;
use common\modules\fts\Module;

class MysqlQuery extends ActiveQuery
{
    public function addSearch($q)
    {
        $q = Module::getInstance()->normalizeQuery($q);
        $weights = Module::getInstance()->weights;

        $rankColumn = "match(title) against(:query in boolean mode) * :title_weight + match(text) against(:query in boolean mode) * :text_weight + match(tags) against(:query in boolean mode) * :tags_weight";
        $rankCondition = "match(title) against(:query in boolean mode) * :title_weight > 0 or match(text) against(:query in boolean mode) * :text_weight > 0 or match(tags) against(:query in boolean mode) * :tags_weight > 0";
        return $this->addSelect(['rank' => new Expression($rankColumn)])
            ->andWhere(new Expression($rankCondition))            
            ->addParams([
                'query' => $q,
                'title_weight' => $weights['title'],
                'text_weight' => $weights['text'],
                'tags_weight' => $weights['tags'],
            ]);
    }

    public function search($q)
    {
        return $this->select('*')->addSearch($q)->orderBy(['rank' => SORT_DESC]);
    }
}