<?php

namespace common\modules\fts\models;

use Yii;
use common\modules\fts\Module;
use common\modules\fts\components\SearchInterface;
use common\modules\fts\models\drivers\MysqlQuery;

/**
 * This is the model class for table "search_index".
 *
 * @property integer $id
 * @property string $model_class
 * @property string $model_id
 * @property string $title
 * @property string $text
 * @property string $tags
 */
class SearchIndex extends \yii\db\ActiveRecord
{
    use SearchDataTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_index';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'tags'], 'string'],
            [['model_class', 'model_id', 'title'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('search', 'ID'),
            'model_class' => Yii::t('search', 'Document Class'),
            'model_id' => Yii::t('search', 'Document ID'),
            'title' => Yii::t('search', 'Document Title'),
            'text' => Yii::t('search', 'Document Text'),
            'tags' => Yii::t('search', 'Document Tags'),
        ];
    }

    public static function find()
    {
        $driver = '';
        switch (Yii::$app->db->driverName) {
            case 'mysql':
            case 'mysqli':
                $driver = MysqlQuery::class;
                break;
            case 'pgsql':
                // $query = PgsqlQuery::class;
                // break;
            default: 
                throw new \Exception('Not supported db driver');
        }
        return new $driver(get_called_class());
    }

    public function getModel()
    {
        if (empty($this->model_class) || empty($this->model_id)) {
            return null;
        }
        return $this->model_class::findOne($this->model_id);
    }

    public function getIndexText()
    {
        return implode(' ', [$this->title, $this->text, $this->tags]);
    }

    public static function getNotIndexedCount($modelClasses = null)
    {
        if ($modelClasses == null) {
            $modelClasses = Module::getInstance()->getModelClasses();
        } elseif (is_string($modelClasses)) {
            $modelClasses = [$modelClasses];
        }
        $totalCount = 0;
        foreach ($modelClasses as $modelClass) {
            $totalCount += $modelClass::find()->where([
                'not in', 
                $modelClass::primaryKey()[0], 
                static::find()->select('model_id')->where(["model_class" => $modelClass])
            ])->count();
        }
        //$indexCount = static::find()->where(['model_class' => $modelClasses])->count();
        return $totalCount;// - $indexCount;
    }

    public static function reindex($modelClasses = null)
    {
        $count = 0;

        if ($modelClasses == null) {
            $modelClasses = Module::getInstance()->getModelClasses();
        } elseif (is_string($modelClasses)) {
            $modelClasses = [$modelClasses];
        }

        foreach ($modelClasses as $modelClass) {
            if ((new \ReflectionClass($modelClass))->implementsInterface(SearchInterface::class)) {
                $models = $modelClass::find();
                foreach($models->all() as $model) {
                    $count += $model->saveSearchData();
                }
            } else {
                throw new \Exception(__METHOD__ . ": " . $modelClass . " must be instance of SearchInterface");
            }
        }

        return $count;
    }

    public static function index($number = null, $modelClasses = null)
    {
        $count = 0;

        if ($modelClasses == null) {
            $modelClasses = Module::getInstance()->getModelClasses();
        } elseif (is_string($modelClasses)) {
            $modelClasses = [$modelClasses];
        }

        foreach ($modelClasses as $modelClass) {
            if ((new \ReflectionClass($modelClass))->implementsInterface(SearchInterface::class)) {
                $models = $modelClass::find()->where([
                    'not in', 
                    $modelClass::primaryKey()[0], 
                    static::find()->select('model_id')->where(["model_class" => $modelClass])
                ]);
                if ($number !== null) {
                    $models->limit($number);
                }
                foreach($models->all() as $model) {
                    $count += $model->saveSearchData();
                }
            } else {
                throw new \Exception(__METHOD__ . ": " . $modelClass . " must be instance of SearchInterface");
            }
        }

        return $count;
    }


    public static function cleanup($modelClasses = null)
    {
        $count = 0;

        if ($modelClasses == null) {
            $modelClasses = Module::getInstance()->getModelClasses();
        } elseif (is_string($modelClasses)) {
            $modelClasses = [$modelClasses];
        }

        foreach ($modelClasses as $modelClass) {
            if ((new \ReflectionClass($modelClass))->implementsInterface(SearchInterface::class)) {
                $searchItems = static::find()
                    ->where(["model_class" => $modelClass])
                    ->andWhere([
                        'not in',
                        'model_id',
                        $modelClass::find()->select($modelClass::primaryKey()[0])
                    ]);
                foreach($searchItems->all() as $searchItem) {
                    $count += $searchItem->delete();
                }
            } else {
                throw new \Exception(__METHOD__ . ": " . $modelClass . " must be instance of SearchInterface");
            }
        }

        return $count;
    }
}
