<?php

namespace common\modules\fts;

use Yii;
use common\modules\fts\components\SearchInterface;
use TheSeer\Tokenizer\Exception;
use Wamania\Snowball\Russian as Stemmer;

class Module extends \yii\base\Module
{
    public $models = [];
    public $queryWildcard = true;
    public $stemming = false;
    public $weights = [
        'title' => 1.0,
        'text' => 0.4,
        'tags' => 0.2,
    ];
    public $useLabels = false;
    public $minTokenSize = 4;
    public $stemmer;

    public function init()
    {
        $this->stemmer = new Stemmer();
        parent::init();
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
        	$this->controllerNamespace = 'common\modules\fts\console';
        }
    }

    public function getModelClasses()
    {
        $result = [];
        foreach($this->models as $model) {
            if (is_string($model)) {
                $result[]= $model;
            } elseif (isset($model['class'])) {
                $result[]= $model['class'];
            } else {
                throw new Exception('Invalid module configuration');
            }
        }
        return $result;
    }

    public function getSearchResult($indexModel)
    {
        $modelClass = $indexModel->model_class;
        foreach($this->models as $model) {
            if (isset($model['class'] && isset($model['class'] == $modelClass && isset($model['widget'])) {
                $widgetClass = $model['widget'];
                return $widgetClass::widget($indexModel->getModel());
            }
        }
    }

    public function normalizeDocument($document)
    {
        $words = $this->words($document);
        foreach($words as &$word) {
            $word = $this->stem(mb_strtolower($word));
        }
        return implode(' ', $words);
    }

    public function normalizeQuery($query)
    {
        $words = $this->words($query);
        $stemmer = new Stemmer();
        foreach($words as &$word) {
            $word = $this->stem(mb_strtolower($word));
            if ($this->queryWildcard) {
                if (mb_substr($word, -1) != '*') {
                    $word .= '*';
                }
            }
        }
        return implode(' ', $words);
    }

    public function words($text)
    {
        return preg_split('#\s|[,.:;!?"\'()]#', $text, -1, PREG_SPLIT_NO_EMPTY);
    }

    public function stem($word)
    {
        $stemmed = $this->stemmer->stem($word);
        if (mb_strlen($stemmed) >= $this->minTokenSize) {
            $word = $stemmed;
        }
        return $word;
    }
}
