<?php

return [
    // Search form and results
    'Search...' => 'Поиск...',
    'Search results' => 'Результаты поиска',
    "Search results for '{query}'" => 'Результаты поиска для "{query}"',
    'Results {begin} - {end} of {totalCount} items' => 'Результаты {begin} - {end} из {totalCount}',
    'Query' => 'Запрос',

    // Admin
    'Search Index' => 'Поисковый индекс',
    'Not indexed: {count}' => 'Не индексировано: {count}',
    'Indexed: {count}' => 'Проиндексировано: {count}',
    'Deleted: {count}' => 'Удалено: {count}',
    '{icon} Index' => '{icon} Индексировать',
    '{icon} Reindex all' => '{icon} Переиндексировать всё',
    '{icon} Delete all' => '{icon} Удалить всё',
    '{icon} Cleanup' => '{icon} Удалить лишнее',

    // Model attributes
    'Document Class' => 'Класс документа',
    'Document ID' => 'ID документа',
    'Document Title' => 'Заголовок документа',
    'Document Text' => 'Текст документа',
    'Document Tags' => 'Теги документа'
];
