# yii2-mysql-search

WIP: Yii2 full text search based on MySQL features

Config:

```
'modules' => [
        'search' => [
            'class' => \antonrybalko\yii2-mysql-search\Module::class,
            'controllerNamespace' => '\antonrybalko\yii2-mysql-search\console',
            'models' => [
                [
                    'class' => \app\models\Article::class,
                    'widget' => \app\components\SearchResultWidget::class
                ]
            ],
        ]
    ],
]
```

Model
```
use antonrybalko\yii2-mysql-search\components\SearchBehavior;
use antonrybalko\yii2-mysql-search\components\SearchInterface;
use antonrybalko\yii2-mysql-search\models\SearchData;

class Article extends \yii\db\ActiveRecord implements SearchInterface
{

    public function behaviors()
    {
        return [
            'search' => SearchBehavior::class,
        ];
    }

    public function getSearchData(): SearchData
    {
        return new SearchData([
            'title' => $this->title,
            'text' => ['description', 'color', 'materials', 'height', 'width', 'length', 'diam'],
            'tags' => $this->tags
        ]);
    }
}

```